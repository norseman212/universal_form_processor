<?php
include_once( 'class-form-processing.php' );

$GLOBALS['form'] = new Form_Script;

if( !isset( $_POST['form_id'] ) ) {
	return;
} 

$form_id = $_POST['form_id'];

switch( $form_id ) {
	case '1':
		$required = array( 'name', 'email' );
		$args = array(
			//'has_captcha' => true,
			'recipients' => array(
				'example@website.com',
			),
			'bcc' => 'bcc-email@website.com',
			'subject' => 'Main Contact Form',
			'thankyou' => 'thanks.php'
		);
		break;

	case '2':
		$required = array( 'name', 'email' );
		$args = array(
			'has_captcha' => true,
			'recipients' => 'example@website.com',
			'subject' => 'Quick Contact Form'
		);
		break;

	default:
		$required = null;
		$args = array(
			'recipients' => 'example@website.com',
			'subject' => 'New Form Submission'
		);
}

$GLOBALS['form'] = new Form_Script( $_POST, $required, $args, $_FILES );