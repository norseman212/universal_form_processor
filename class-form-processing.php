<?php

class Form_Script {

	public $form_id = null;

	public $fields = array();

	public $missing = array();

	public $validation_error = null;

	public $message = '';

	public $success = false;

	public $thank_you = null;

	public $files = array();

	private $_form_valid = false;

	private $_has_captcha = false;

	private $_recipients;

	private $_bcc;

	private $_subject;

	private $_files;

	private $_uid;

	protected $secret_key = 'XXXXXXXXXXX';

	/**
	 * Construct the Form Script class
	 * @access public
	 * @param array[] $post
	 * @param array[] $required
	 * @param array[] $args
	 */
	public function __construct( $post = array(), $required = array(), $args = array(), $files = array() ) {

		$this->_uid = md5( uniqid( time() ) );

		if( isset( $args['has_captcha'] ) ) {
			$this->_has_captcha = $args['has_captcha'];
		}

		if( array_key_exists( 'g-recaptcha-response', $post ) && !$this->verify_captcha( $post['g-recaptcha-response'] ) && $this->_has_captcha ) {
			return;
		}

		if( is_array( $files ) && !empty( $files ) ) {
			$this->_files = $files;
		}

		if( !empty( $post ) ) {
			$this->form_id = $post['form_id'];
			$this->set_fields( $post );
		}		
		
		$this->_process_files( $files );		
		$this->_validate_fields( $required );

		if( $this->_form_valid ) {
			$this->set_message();

			$this->_recipients = $args['recipients'];
			$this->_subject = $args['subject'];

			if( isset( $args['bcc'] ) && $args['bcc'] != '' ) {
				$this->_bcc = $args['bcc'];
			}

			if( isset( $args['thankyou'] ) && $args['thankyou'] != '' ) {
				$this->thank_you = $args['thankyou'];
			}

			$this->send_mail();			

		}

	}

	/**
	 * Proccess the Files sent with the form
	 * @access private
	 * @param array[] $files
	 */
	private function _process_files( $files ) {
		if( empty( $files ) ) {
			return;
		}

		foreach ( $files as $file ) {
			if( is_uploaded_file( $file['tmp_name'] ) ) {
				$attachment = fopen( $file['tmp_name'], "rb" );
				$data = fread( $attachment, filesize( $file['tmp_name'] ) );
				fclose( $attachment );
				$data = chunk_split( base64_encode( $data ) );
				$this->files[] = array( 
					'type' => $file["type"], 
					'name' => $file["name"],
					'data' => $data 
				);
			}
		}

	}

	/**
	 * verify the captcha
	 * @access private
	 * @param string $site_key
	 */
	private function verify_captcha( $site_key ) {
		$postdata = http_build_query( array( 'secret' => $this->secret_key, 'response' => $site_key ) );
		$opts = array( 'http' => array(
			'method'  => 'POST',
			'header'  => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata
			)
		);
		$context  = stream_context_create( $opts );
		$result = file_get_contents( 'https://www.google.com/recaptcha/api/siteverify', false, $context );
		$check = json_decode( $result );

		return $check->success;
	}

	/**
	 * Set the fields for the class
	 * @access public
	 * @param array[] $post
	 */
	public function set_fields( $post ) {

		foreach( $post as $key => $value ) {
			$label = $this->make_label( $key );
			$fields[$key] = array( 'label' => $label, 'value' => strip_tags( $value ) );
		}

		$fields = $this->checkbox_check( $fields );
		$fields['submission_date'] = array( 'label' => 'Submission date', 'value' => date( "F d, Y" ) );

		$this->fields = $fields;

	}

	/**
	 * Check to see if any fields are checkboxes
	 * @access private
	 * @param array[] $fields
	 */
	private function checkbox_check( $fields ) {
		$keys = array_keys( $fields );
		$array = array();
		foreach( $keys as $key ) {
			$key_array = explode( '__', $key );
			if( count( $key_array ) > 1 ) {
				$group_name = $key_array[0];
				$unset_keys[] = $key;
				$groups[$group_name][] = $fields[$key]["value"];
			}	
		}

		if( !empty( $unset_keys ) && !empty( $groups ) ) {
			foreach( $unset_keys as $unset_key ) {
				unset( $fields[$unset_key] );
			}

			foreach( $groups as $key => $value ) {
				$value = implode( ', ', $value );
				$label = $this->make_label( $key );
				$fields[$key] = array( 'label' => $label, 'value' => $value );
			}
		}
		return $fields;

	}

	/**
	 * Create a label for the fields array
	 * @access private
	 * @param array[] $key
	 */
	private function make_label( $key ) {
		$label = $key;
		$first_letter = str_split( $label )[0];
		$first_letter = strtoupper( $first_letter );

		$label = str_split( $label );
		$null = array_shift($label);
		$label = implode( '', $label );
		$label = $first_letter . $label;
		$label = str_replace( '_', ' ', $label );

		return $label;
	}

	/**
	 * Checks to make sure that the required fields are not blank
	 * @access private
	 * @param array[] $required_keys
	 */
	private function _validate_fields( $required_keys ) {
		if( empty( $required_keys ) ) {
			return;
		}

		foreach( $required_keys as $required ) {

			$key_check = $this->fields[$required];
			if( !isset( $key_check['value'] ) || $key_check['value'] === '' ) {
				$this->missing[] = $this->fields[$required]['label'];
			}
		}

		if( !empty( $this->missing ) ) {
			$validation_error = 'There was a problem with your submission. The following fields were left empty:' . "\n\n";

			$validation_error .= '<ul>';
			foreach( $this->missing as $field ) {
				$validation_error .= '<li>' . $field . '</li>';
			}
			$validation_error .= '</ul>';

			$this->validation_error = $validation_error;
			$this->_form_valid = false;
			$this->success = false;
		} else {
			$this->_form_valid = true;
		}

	}

	/**
	 * Create the message that is sent in the email
	 * @access private
	 */
	private function set_message() {

		unset( $this->fields['form_id'] );
		if( $this->_has_captcha ) {
			unset( $this->fields['g-recaptcha-response'] );
		}

		$eol = "\r\n"; 

		$this->message .= "--" . $this->_uid . $eol;
		$this->message .= "Content-Type: text/html; charset=US-ASCII" . $eol;
    	$this->message .= "Content-Transfer-Encoding: 7bit" . $eol . $eol;

		$this->message .= '<table width="100%">';
		foreach( $this->fields as $field ) {
			$this->message .= '<tr><td width="25%" valign="top"><strong>' . $field["label"] . '</strong></td><td valign="top">' . $field["value"] . '</td></tr>';
		}
		$this->message .= '<table>' . $eol . $eol;

		$this->_attach_files();	
	}

	/**
	 * Attach the uploaded files
	 * @access private
	 */
	private function _attach_files() {
		$eol = "\r\n"; 
		if( empty( $this->files ) ) {
			$this->message .= "--" . $this->_uid . "--" . $eol;
		} else {
			$this->message .= "--" . $this->_uid . $eol. $eol;
		}

		$total_files = count( $this->files );
		$i = 0;

		foreach( $this->files as $file ) {
			$name = $file['name'];
			$type = $file['type'];
			$data = $file['data'];
			$i++;

			$this->message .= "--" . $this->_uid . $eol;
			$this->message .= "Content-Type: application/octet-stream; name=\"" . $name . "\"" . $eol;
			$this->message .= "Content-Transfer-Encoding: base64" . $eol;
    		$this->message .= "Content-Disposition: attachment; filename=\"".$name."\"\r\n\r\n";
    		$this->message .= $data . $eol . $eol;

    		if( $i === $total_files ) {
				$this->message .= "--" . $this->_uid . "--" . $eol;
    		} else {
    			$this->message .= "--" . $this->_uid . $eol;
    		}
			
		}

	}

	/**
	 * Create send the email
	 * @access private
	 */
	private function send_mail() {
		$eol = "\r\n";

		$headers = "From: Test <donotreply@test.com>" . $eol;
		if( isset( $this->_bcc ) ) {
			$headers .= "Bcc: " . $this->_bcc . $eol;	
		}		
		$headers .= "MIME-Version: 1.0" . $eol;
		$headers .= "Content-Type: multipart/mixed; boundary=\"" . $this->_uid . "\"" . $eol;
		$headers .= "This is a MIME encoded message." . $eol;
		

		if( is_array( $this->_recipients ) ) {
			$this->_recipients = implode( ', ', $this->_recipients );
		}

		$this->success = true;	
		mail( $this->_recipients, $this->_subject, $this->message, $headers );

		if( $this->success && $this->thank_you ) {
			header( 'Location: ' . $this->thank_you );
		}
			
	}
}