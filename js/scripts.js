

var $nav = $('.nav');
var $navItem = $('.nav li');
var navOffsetNumber = 2;
var winWidth = $(window).width();

var $items = getNavItems( $navItem );
var dropdownMenu = makeNavDropDown( $items );

$(window).resize( function() {
	winWidth = $(window).width();	

	if( winWidth > 768 ) {
		$('.dropdown.open').removeClass( 'open' );
	}

} );

hideNavItems( $navItem, navOffsetNumber );
addDropDown( $navItem, dropdownMenu, navOffsetNumber );



function addDropDown( navItemElem, dropdown, num ) {
	var z = 1;
	navItemElem.each( function() {
		if( z === num ) {
			$(this).after( '<li class="more-toggle"><a class="dropdown-toggle" href="#!">More</a>' + dropdown + '</li>' );
		}
		z++;
	} );
}


function hideNavItems( navItemElem, num ) {
	var z = 1;
	navItemElem.each( function() {
		if( z > num ) {
			$(this).addClass( 'hidden-mobile' );
		}

		z++;
	} );
}


function getNavItems( navItemElem ) {
	var $items = [];
	var z = 0;

	navItemElem.each( function() {
		var title = $(this).text();
		var link = $(this).find('a').attr( 'href' )
		var itemObject = { title: title, link: link };

		$items[z] = itemObject;
		z++;
	});

	return $items;
}

function makeNavDropDown( items ) {
	var output = '<ul class="dropdown">';
	for (var i = items.length - 1; i >= navOffsetNumber; i--) {
		var item = items[i];
		output += '<li><a href="' + item.link + '">' + item.title + '</a></li>';
		console.log( item );
	}
	output += '<ul>';

	return output;
}

$('.dropdown-toggle').click( function(e) {
	e.preventDefault();
	var dropdown = $(this).next( '.dropdown' );
	dropdown.toggleClass( 'open' );
} );
	


//store each LI text and link in an object
//all elements after a set number are hidden
//then the list is output as a dropdown

//# sourceMappingURL=scripts.js.map