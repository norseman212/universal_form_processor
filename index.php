<?php
include_once( 'header.php' );

?>
<form action="/" method="POST" enctype="multipart/form-data">
	<div class="validation_error"><?php echo $form->validation_error; ?></div>
	<input type="hidden" name="form_id" value="1">
	<ul>
		<li>
			<label for="input--name">Name*</label>
			<input id="input--name" type="text" name="name" >
		</li>
		<li>
			<label for="input--phone">Phone</label>
			<input id="input--phone" type="tel" name="phone">
		</li>
		<li>
			<label for="input--email">Email*</label>
			<input id="input--email" type="email" name="email" >
		</li>
		<li>
			<label>Services</label>
			<ul>
				<li>
					<input id="input--services__web-design" type="checkbox" name="services__web-design" value="Web Design">
					<label for="input--services__web-design">Web Design</label>
				</li>
				<li>
					<input id="input--services__web-dev" type="checkbox" name="services__web-dev" value="Web Development">
					<label for="input--services__web-dev">Web Development</label>
				</li>
				<li>
					<input id="input--services__content-strat" type="checkbox" name="services__content-strat" value="Content Strategy">
					<label for="input--services__content-strat">Content Strategy</label>
				</li>
			</ul>
		</li>
		<li>
			<label for="input--comments">Comments</label><br>
			<textarea name="comments" id="input--comments" cols="30" rows="10"></textarea>
		</li>
		<li>
			<label for="input--file">File</label><br>
			<input name="file" type="file">
		</li>
		<li>
			<?php //<div class="g-recaptcha" data-sitekey="XXXXXXXX"></div> ?>
		</li>	
	</ul>	
	<input type="submit">

</form>


<?php include( 'footer.php' ); ?>